# Sidekick
This is just an api-gateway for the challange-me app

## Requirements

* [Docker Desktop 3.x](https://www.docker.com/products/docker-desktop)
* [PHP 7.4](https://www.php.net/downloads.php)
    * [Composer 1.3.2](https://getcomposer.org/)
    * [Slim 3.x](https://www.slimframework.com/docs/v3/)
* [mongodb 1.8](https://docs.mongodb.com/guides/)

# Installation

> **Attention**: for now you have to use the same php version (!) on your local machine as the docker container 
> (because of "composer"). To install the extension:
>
> ```sudo pecl install mongodb```
>
> than add ```extension=mongodb.so``` to your php.ini


## run local environment

```bash
> cp ./docker/example.env .env
> docker-compose up
```

### nginx & php
http://localhost:8888/info.php

For testing the Rest-API in your IDE: `./docs/Checkin-Rest-API-V0.1.http`

### mongodb
http://localhost:27017
 
For the initial use you need to create the database `lima` first! I recommend [Robo3T](https://robomongo.org/) for 
observing your MongoDB.



## build your app

```bash
> cd ./application
> cp example.env .env
> composer install
```