<?php

namespace Checkin\Utils;

use Dotenv\Dotenv;

class Settings {
    
    private $dotenv;
    const CONFIG_NAME = ".env";
    
    
    public function __construct() {
        $this->dotenv = Dotenv::createImmutable(__DIR__ . '/../..', self::CONFIG_NAME);
        $this->dotenv->load();
    }
    
    
    /**
     * get value from env file
     * @param $name
     * @return mixed
     */
    public function get($name) {
        return $_ENV[$name];
    }
    
}