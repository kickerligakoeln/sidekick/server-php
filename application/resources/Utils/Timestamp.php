<?php

namespace Checkin\Utils;

class Timestamp {


  /**
   * @return string
   */
  public static function current(): string {
    date_default_timezone_set('Europe/Berlin');
    $time = new \DateTime();

    return $time->format('c');
  }


  /**
   * @param $minutes
   * @return string
   */
  public static function sub($minutes): string {
    date_default_timezone_set('Europe/Berlin');
    $date = new \DateTime();
    $date->sub(new \DateInterval('PT' . $minutes . 'M'));

    return $date->format('c');
  }
}