<?php

namespace Checkin\Utils;

class UUID {
    
    /**
     * @return string
     */
    public static function create(): string {
        return uniqid(true);
    }
    
}