<?php

namespace Checkin\Utils;

class Cookie {
    
    /**
     * @param string $value
     * @param string|null $name
     * @return bool
     */
    public static function set(string $value, $name = 'access_token') {
        return setcookie($name, $value, time() + 3600 * 24, '/');
    }
    
}