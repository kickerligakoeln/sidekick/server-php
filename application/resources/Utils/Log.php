<?php

namespace Checkin\Utils;

class Log {
    
    
    /**
     * @param $val
     */
    public static function send($val) {
        if(is_string($val)) {
            file_put_contents('php://stderr', "[" .date("d/M/Y|H:i:s"). "] - " . $val . "\n");
        } else {
            file_put_contents('php://stderr', print_r($val, true));
        }
    }
    
}