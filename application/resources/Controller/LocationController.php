<?php

namespace Checkin\Controller;

use Checkin\Database\Kickerkarte;
use Checkin\Utils\Log;
use Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class LocationController {
    
    protected $request;
    protected $response;
    protected $args;
    
    protected $db;
    
    public function __construct($req, $resp, $args) {
        $this->request = $req;
        $this->response = $resp;
        $this->args = $args;
        
        $this->db = new Kickerkarte();
    }
    
    public static function register($app) {
        
        $app->group('/v1', function() use ($app) {
            $app->group('/location', function() use ($app) {
                
                // get locations for lat|long
                $app->post('/recommendations', function(Request $request, Response $response, array $args) {
                    $controller = new LocationController($request, $response, $args);
                    $allPostPutVars = $request->getParsedBody();
                    return $controller->getRecommendations($allPostPutVars);
                });
            });
        });
    }
    
    
    /**
     * get recommendation for current geo data
     * @param array $data
     * @return \Slim\Http\Response
     */
    private function getRecommendations(array $data): \Slim\Http\Response {
        $output = $this->db->locations(array(
            'latitude' => $data['coords']['latitude'],
            'longitude' => $data['coords']['longitude'],
            'distance' => $data['distance'],
            'getDetails' => true
        ));
    
        array_unshift($output, array(
            'name' => 'Private',
            'url' => 'private',
            'latitude' => $data['coords']['latitude'],
            'longitude' => $data['coords']['longitude'],
            'distance' => 0
        ));
        
        return $this->response->withJson($output, 200);
    }
    
}
