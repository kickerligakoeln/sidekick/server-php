<?php

namespace Checkin\Controller;

use Checkin\Utils\Cookie;
use Checkin\Utils\Log;
use Checkin\Utils\UUID;
use Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Checkin\Database\MongoDB;
use Checkin\Utils\Timestamp;

class CheckinController {

    protected $request;
    protected $response;
    protected $args;

    protected $db;

    const ACTIVE_DURATION = 60; // minutes

    public function __construct($req, $resp, $args) {
        $this->request = $req;
        $this->response = $resp;
        $this->args = $args;

        $this->db = new MongoDB();
    }


    /**
     * @param $app
     * @throws Exception
     */
    public static function register($app) {

        $app->group('/v1', function() use ($app) {
            $app->group('/checkin', function() use ($app) {

                // create
                $app->post('', function(Request $request, Response $response, array $args) {
                    $controller = new CheckinController($request, $response, $args);
                    $allPostPutVars = $request->getParsedBody();
                    $cookieVar = $request->getCookieParam("access_token");

                    return $controller->insert($allPostPutVars, $cookieVar);
                });

                // get all active checkins
                $app->get('', function(Request $request, Response $response, array $args) {
                    $controller = new CheckinController($request, $response, $args);
                    return $controller->find();
                });

                // get single checkin
                $app->get('/{uuid}', function(Request $request, Response $response, array $args) {
                    $controller = new CheckinController($request, $response, $args);
                    return $controller->findOne($args['uuid']);
                });

                // update checkin
                $app->put('', function(Request $request, Response $response, array $args) {
                    $controller = new CheckinController($request, $response, $args);
                    $allPostPutVars = $request->getParsedBody();
                    return $controller->update($allPostPutVars);
                });

                // remove checkin
                $app->delete('/{uuid}', function(Request $request, Response $response, array $args) {
                    $controller = new CheckinController($request, $response, $args);
                    return $controller->delete($args['uuid']);
                });
            });
        });
    }


    /**
     * @param array $data
     * @param string|null $auth
     * @return \Slim\Http\Response
     * @throws Exception
     */
    private function insert(array $data, ?string $auth = null): \Slim\Http\Response {
        if(!$data['coords']) {
            throw new Exception("Missing required 'coords' attributes.", 400);
        }

        $data['create'] = Timestamp::current();

        // todo: fetch cookie in be
        // cannot read cookie from request on localhost
        // $data['user'] = array( "id" => $auth );
        if(!isset($data['user']['id'])) {
            $data['user']['id'] = UUID::create();
        }

        // TODO: check for "active" checkin first
        $output = $this->db->create($data);

        return $this->response->withJson($output, 200);
    }


  /**
   * @return \Slim\Http\Response
   * @throws Exception
   */
    private function find(): \Slim\Http\Response {
      $query = array(
          'create' => array(
              '$gte' => Timestamp::sub(self::ACTIVE_DURATION)
          )
      );
      $ouput = $this->db->read(null, $query);

      return $this->response->withJson($ouput, 200);
    }


    /**
     * @param string|null $uuid
     * @return \Slim\Http\Response
     * @throws Exception
     */
    private function findOne(?string $uuid = null): \Slim\Http\Response {
        $output = $this->db->read($uuid);
        return $this->response->withJson($output, 200);
    }


    /**
     * update checkin
     * @param array $data
     * @return \Slim\Http\Response
     */
    private function update(array $data): \Slim\Http\Response {

        try {
            if(!$data['uuid']) {
                throw new Exception("Missing required 'uuid' attribute.", 400);
            }

            $output = $this->db->update($data);
            $newResponse = $this->response->withJson($output, 200);
        } catch(Exception $ex) {
            $responseData = array('message' => $ex->getMessage());
            $newResponse = $this->response->withJson($responseData, $ex->getCode());
        }

        return $newResponse;
    }


    /**
     * @param string $uuid
     * @return \Slim\Http\Response
     */
    private function delete(string $uuid): \Slim\Http\Response {
        try {
            $output = $this->db->delete($uuid);
            $newResponse = $this->response->withJson([], 200);
        } catch(Exception $ex) {
            $responseData = array('message' => $ex->getMessage());
            $newResponse = $this->response->withJson($responseData, $ex->getCode());
        }

        return $newResponse;
    }
}
