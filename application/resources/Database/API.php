<?php

namespace Checkin\Database;

interface API {

  /**
   * @param array $data
   * @return array
   */
  function fetch($uri, array $data);
}
