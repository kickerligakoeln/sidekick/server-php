<?php

namespace Checkin\Database;

use Exception;
use Checkin\Utils\Settings;
use MongoDB\Client as Mongo;
use MongoDB\BSON\ObjectId as MongoId;
use MongoDb\Collection;

class MongoDB implements Database {

  private $collection;

  public function __construct() {
    $env = new Settings();

    $host = $env->get('DB_HOST');
    $port = $env->get('DB_PORT');
    $auth = (true) ? "" : "user:pwd@";

    try {
      $mongodb = new Mongo('mongodb://' . $auth . $host . ':' . $port);
      $db = $mongodb->lima;
      $this->collection = $db->selectCollection("checkin");
    } catch(MongoDBDriverExceptionException $e) {
      echo $e->getMessage();
      echo nl2br("n");
    }
  }


  /**
   * @param $id
   * @return bool
   */
  private function validateId(string $id): bool {
    if($id instanceof \MongoDB\BSON\ObjectID) {
      return true;
    }

    try {
      new \MongoDB\BSON\ObjectID($id);
      return true;
    } catch(\Exception $e) {
      return false;
    }
  }


  /**
   * @param array $data
   * @return array
   * @throws Exception
   */
  public function create(array $data): array {
    try {
      $this->collection->insertOne($data);
      return $data;
    } catch(\Exception $e) {
      throw new Exception("Cannot create data", 400);
    }
  }


  /**
   * @param string|null $id
   * @return array
   * @throws Exception
   */
  public function read(?string $id = null, $query = array()): array {
    if(isset($id)) {
      if($this->validateId($id)) {
        $resp = [
            $this->collection->findOne(array(
                "_id" => new MongoId($id)
            ))
        ];
      } else {
        throw new Exception("'" . $id . "' is invalid", 400);
      }
    } else {
      $resp = $this->collection->find($query)->toArray();
    }

    return ($resp) ? $resp : [];
  }


  /**
   * @param array $data
   * @return array[]
   */
  public function update(array $data): array {
    return $data;
  }


  /**
   * @param string $uuid
   * @return bool
   */
  public function delete(string $uuid): bool {
    return true;
  }
}