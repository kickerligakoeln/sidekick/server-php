<?php

namespace Checkin\Database;

use Checkin\Utils\Log;
use Checkin\Utils\Settings;

class Kickerkarte implements API {
    
    private $host;
    
    function __construct() {
        $env = new Settings();
        
        $this->host = $env->get('KICKERKARTE_HOST');
    }
    
    
    /**
     * fetch data from endpoint
     * @param $uri
     * @param array $data
     * @return mixed
     */
    function fetch($uri, array $data) {
        $curl = curl_init();
        $url = sprintf("%s?%s", $uri, http_build_query($data));
        
        curl_setopt($curl, CURLOPT_VERBOSE, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        
        $result = curl_exec($curl);
        
        curl_close($curl);
        
        return json_decode($result, true);
    }
    
    
    /**
     * return all locations
     * @param array $data
     * @return array|mixed
     */
    function locations(array $data) {
        return $this->fetch($this->host . '/location', $data);
    }
}
