<?php

namespace Checkin\Database;

interface Database {
    
    /**
     * @param array $data
     * @return array
     */
    public function create(array $data): array;
    
    /**
     * @param string|null $id
     * @return array
     */
    public function read(?string $id): array;
    
    /**
     * @param array $data
     * @return array
     */
    public function update(array $data): array;
    
    /**
     * @param string $uuid
     * @return bool
     */
    public function delete(string $uuid): bool;
}