<?php

namespace Checkin\Enums;

class CheckinState {
    
    const IN_PROGRESS = "IN_PROGRESS";
    const CANCELED = "CANCELED";
    const DONE = "DONE";
    
    
    /**
     * @return string[]
     */
    static function collection(): array {
        return array(self::IN_PROGRESS, self::CANCELED, self::DONE);
    }
}