<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: POST, GET, OPTIONS, DELETE, PUT");

require './vendor/autoload.php';

use Checkin\Controller\CheckinController;
use Checkin\Controller\LocationController;
use Slim\App;

// init Slim Framework
$config = array(
    'settings' => array(
        'displayErrorDetails' => true
    )
);

$app = new App($config);

// register routes
CheckinController::register($app);
LocationController::register($app);

$app->run();
